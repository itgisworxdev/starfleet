## Welcome to the ItGISworx recruitment project! ##

### What is this repository for? ###
------------------------------------

This repo serves to test knowledge of web technologies for people wanting to join our team.  
The mini project outlined below seeks to test skills in aspects of full stack web development, including:

* data retrieval from an API
* data storage in a database
* data presentation
* troubleshooting


### How do I get set up? ###
----------------------------
#### Create Bitbucket account:
If you already don't have bitbucket account, create one [here](https://bitbucket.org/account/signup/).

#### Create an empty repository:
Login to your bitbucket account and [create a new repository](https://support.atlassian.com/bitbucket-cloud/docs/create-a-git-repository/).

#### Clone this repo:
```bash
git clone https://itGISworx@bitbucket.org/itgisworxdev/starfleet.git
```
#### Change the cloned repository's remote to the URL of your repository:
```bash
cd starfleet && git remote set-url origin https://<your_user_name@bitbucket.org/<your_project>/<your_repo>.git
```
#### Merge the code into your new repo:
```bash
git pull --allow-unrelated-histories
```
#### Create a new readme file and push the changes to your repo:
```bash
echo "### My starfleet project's README" > My_README.md && git add . && git commit -m"Initial commit" && git push
```

### Mini Project ###
--------------------
```bash
echo "Your mission, commander, should you choose to accept it..."
```

#### Get the data
* Write a backend service to pull __**all**__ the data records from [https://gorest.co.in/public/v1/users](https://gorest.co.in/public/v1/users)
* Store the retrieved data in a sqlite database.

#### Present the data
* Retrieve data from the your sqlite database
* Present in web-based (html) frontend.  
* In the data presented, add an additional field that shows the result of each record's ```id/(id-50)```

#### Upload your solution in and tell us about it
* Create a README file with instructions for installation/usage of your awesome solution (We need to be able to get it up and running our side to see!)
* Commit and push your code to the git repo you created.
* Drop us a mail at info@itgisworx.co.za with the share link to your repo.

For the backend, you're welcome to implement the server-side component in any language of your choice e.g. Python, C#, Java, Perl, .net, Rails etc.  
(Browny points for using Python or Perl)

For the web-frontend, you're welcome to use any web-tech e.g. Vue, JQuery, Bootstrap, React etc.